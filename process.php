<?php

$htmlDir = "./html";
$articleDir = "./articles";
$docDir = "./docx";


if ($handle = opendir($docDir)) {
    try {
        while (false !== ($entry = readdir($handle))) {
            
            
            $condition = 1;//$entry === 'Career options with Adwords certification.html';//1;//simple debug condition
            
            if ($condition && preg_match('/.*\.doc(?:x)?/', $entry)) {
            
                $name = substr($entry, 0, strrpos($entry, '.'));
                $filename = "$htmlDir/$name.html";
                
                if (!file_exists($filename)) {
                    //echo "$filename does not exist", PHP_EOL;
                    //continue;
                    throw new Exception("$filename does not exist");
                } else {
                    //continue;
                }
                
                echo "processing $name.html...", PHP_EOL;
                $dom = new DOMDocument;
                @$dom->loadHTMLFile($filename);

                $dom->preserveWhiteSpace = false;

                $nodes = $dom->getElementsByTagName('body')->item(0)->childNodes;
                $content = process_nodes($nodes, false);
                
                $content = filter_dumb_tags($content);
                $content = kill_breaks($content);
                
                //var_dump($content);
                
                $filenameArticle = "$articleDir/$name.html";
                file_put_contents($filenameArticle, $content);
            }
        }
    
    } catch (Exception $e) {
        
        $message = PHP_EOL . $e->getMessage().' - '. $e->getFile(). ':'. $e->getLine(). PHP_EOL;
        die($message);
    }
}

function filter_dumb_tags($content) {
    return strtr($content, array(
        '<p><p class="em">' => PHP_EOL.'<p class="em">',
        '<p><p class="bold">' => PHP_EOL.'<p class="bold">',
        '<p class="bold"><p class="bold">' => PHP_EOL.'<p class="bold">',
        '</p></p>'      => '</p>'
    ));
}

function kill_breaks($content) {
    return $content;
}

function process_nodes(DOMNodeList $nodes, $parent = false) {
    $html = "";
    
    if (false === $parent) {
        $titled = false;
    }
    
    foreach ($nodes as $key => $node) {
        
        $tagName = "";
        $className = "";
        
        if ($node instanceof DOMElement) {

            if (false === $parent && !$titled && strlen($node->nodeValue)) {//append title
                $tagName = "p";
                $className = "bold";
                $titled = true;
            } else {
                
                switch ($node->tagName) {
                    
                    case 'b':
                    case 'strong':
                        $tagName = "p";
                        $className = "bold";
                        break;
                    
                    case 'em':
                    case 'i':
                        $tagName = "p";
                        $className = "em";
                        break;
                    
                    default:
                        if (preg_match("/h[1-6]{1}/", $node->tagName)) {
                            $tagName = "p";
                            $className = "bold";
                        }
                        break;
                }
            }
        
        }
        
        
        $html.= process_node($node, $parent, $tagName, $className);
    }
    return $html;
}

function process_node($node, $parent, $tagName = '', $className = '') {
    
    if ($node instanceof DOMElement) {
        
        $tagName = strlen($tagName) ? $tagName : $node->tagName;
        
        if (in_array($parent, array('ul','ol'))) {
            $tagName = "li";
        }
        
        $html = "";
        
        $tags_single = array(
            'br',
        );

        $tags_allowed = array(
            'h1', 
            'h2', 
            'h3', 
            'h4', 
            'h5', 
            'h6', 
            'p', 
            'i', 
            'b',
            'strong',
            'em', 
            'ul', 
            'li', 
            'ol',
        );

        if (in_array($tagName, $tags_allowed)) {
            
            $attrs = "";
            if ($className) {
                $attrs = ' class="'.$className.'"';
            }

            if (in_array($tagName, $tags_single)) {
                $html.= "<$tagName{$attrs}/>";
            } else {
                $html.= "<$tagName{$attrs}>";
            }    
        }

        if ($node->hasChildNodes()) {
            $html.= process_nodes($node->childNodes, $tagName);
        }
        
        if (in_array($tagName, $tags_allowed) 
                && !in_array($tagName, $tags_single)) {
            $html.= "</$tagName>";
            
            if (!strlen(strip_tags($html))) {//filter empty tags
                $html = "";
            }
        }
            
        
        
    } else {//DomText
        $html = $node->wholeText;
        $html = trim(preg_replace("/\r\n|\r|\n/", ' ', $html));
        $html = trim(preg_replace("/\s+/", ' ', $html));
    }
    
    return $html;
}